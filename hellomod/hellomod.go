package hellomod

import "fmt"

func SayHello() {
	fmt.Println("Hello from hellomod module (v1.0.1)")
}
